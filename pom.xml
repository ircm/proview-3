<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <parent>
    <groupId>ca.qc.ircm</groupId>
    <artifactId>parent</artifactId>
    <version>7</version>
    <relativePath>../parent/pom.xml</relativePath>
  </parent>
  <modelVersion>4.0.0</modelVersion>
  <groupId>ca.qc.ircm.proteomique</groupId>
  <artifactId>proview</artifactId>
  <name>ProView</name>
  <version>3.1-SNAPSHOT</version>
  <inceptionYear>2006</inceptionYear>
  <packaging>war</packaging>
  <description>LIMS managing sample analysis by mass spectrometry (MS)</description>
  <url>http://bitbucket.org/ircm/proview-3</url>
  <licenses>
    <license>
      <name>GNU Affero General Public License</name>
      <url>https://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:https://bitbucket.org/ircm/proview-3.git</connection>
    <developerConnection>scm:git:https://bitbucket.org/ircm/proview-3.git</developerConnection>
    <url>https://bitbucket.org/ircm/proview-3</url>
  </scm>
  <issueManagement>
    <system>Bitbucket</system>
    <url>https://bitbucket.org/ircm/proview-3/issues</url>
  </issueManagement>
  <properties>
    <main-class>ca.qc.ircm.proview.Main</main-class>
    <javac.version>1.8</javac.version>
    <spring-boot.version>2.3.4.RELEASE</spring-boot.version>
    <querydsl.version>4.2.1</querydsl.version>
    <shiro.version>1.3.2</shiro.version>
    <vaadin.version>14.6.8</vaadin.version>
    <scrolllayout.version>2.0.2</scrolllayout.version>
    <guava.version>25.0-jre</guava.version>
    <commons-lang3.version>3.9</commons-lang3.version>
    <commons-email.version>1.5</commons-email.version>
    <mariadb-java-client.version>2.5.1</mariadb-java-client.version>
    <thymeleaf.version>3.0.11.RELEASE</thymeleaf.version>
    <spotbugs.version>4.0.0-beta1</spotbugs.version>
    <generate-property-names.version>0.5</generate-property-names.version>
    <karibu-testing.version>1.3.0</karibu-testing.version>
    <greenmail.version>1.6.1</greenmail.version>
    <testbench.skip>false</testbench.skip>
    <testbench.maxAttempts>3</testbench.maxAttempts>
    <testbench.driver>org.openqa.selenium.chrome.ChromeDriver</testbench.driver>
    <apt-maven-plugin.version>1.1.3</apt-maven-plugin.version>
    <migrations-maven-plugin.version>1.1.3</migrations-maven-plugin.version>
  </properties>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-bom</artifactId>
        <version>${vaadin.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.thymeleaf</groupId>
        <artifactId>thymeleaf</artifactId>
        <version>${thymeleaf.version}</version>
      </dependency>
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>${spring-boot.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <groupId>junit</groupId>
          <artifactId>junit</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.unboundid</groupId>
      <artifactId>unboundid-ldapsdk</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-testbench</artifactId>
      <scope>test</scope>
      <exclusions>
        <!-- Webjars are only needed when running in Vaadin 13 compatibility mode -->
        <exclusion>
          <groupId>com.vaadin.webjar</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.insites</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.polymer</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.polymerelements</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.vaadin</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.webcomponents</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.github.mvysny.kaributesting</groupId>
      <artifactId>karibu-testing-v10-spring</artifactId>
      <version>${karibu-testing.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.icegreen</groupId>
      <artifactId>greenmail</artifactId>
      <version>${greenmail.version}</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <groupId>junit</groupId>
          <artifactId>junit</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.icegreen</groupId>
      <artifactId>greenmail-junit5</artifactId>
      <version>${greenmail.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-email</artifactId>
      <version>${commons-email.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-configuration-processor</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-hateoas</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-acl</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-ldap</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-validation</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-mail</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.apache.tomcat</groupId>
      <artifactId>tomcat-jdbc</artifactId>
    </dependency>
    <dependency>
      <groupId>org.yaml</groupId>
      <artifactId>snakeyaml</artifactId>
    </dependency>
    <dependency>
      <groupId>com.querydsl</groupId>
      <artifactId>querydsl-core</artifactId>
    </dependency>
    <dependency>
      <groupId>com.querydsl</groupId>
      <artifactId>querydsl-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.shiro</groupId>
      <artifactId>shiro-core</artifactId>
      <version>${shiro.version}</version>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-core</artifactId>
      <exclusions>
        <!-- Webjars are only needed when running in Vaadin 13 compatibility mode -->
        <exclusion>
          <groupId>com.vaadin.webjar</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.insites</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.polymer</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.polymerelements</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.vaadin</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.webcomponents</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-spring-boot-starter</artifactId>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-confirm-dialog-flow</artifactId>
      <exclusions>
        <!-- Webjars are only needed when running in Vaadin 13 compatibility mode -->
        <exclusion>
          <groupId>com.vaadin.webjar</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.insites</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.polymer</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.polymerelements</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.vaadin</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.webjars.bowergithub.webcomponents</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>ch.carnet.kasparscherrer</groupId>
      <artifactId>scrolllayout</artifactId>
      <version>${scrolllayout.version}</version>
    </dependency>
    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
      <version>${guava.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>org.thymeleaf</groupId>
      <artifactId>thymeleaf</artifactId>
    </dependency>
    <dependency>
      <groupId>com.github.spotbugs</groupId>
      <artifactId>spotbugs-annotations</artifactId>
      <version>${spotbugs.version}</version>
    </dependency>
    <dependency>
      <groupId>ca.qc.ircm</groupId>
      <artifactId>generate-property-names</artifactId>
      <version>${generate-property-names.version}</version>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.mariadb.jdbc</groupId>
      <artifactId>mariadb-java-client</artifactId>
      <scope>runtime</scope>
    </dependency>
  </dependencies>
  <build>
    <testResources>
      <testResource>
        <directory>src/test/resources</directory>
      </testResource>
      <testResource>
        <directory>src/test/resources</directory>
        <includes>
          <include>**/*.sql</include>
        </includes>
        <filtering>true</filtering>
      </testResource>
    </testResources>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <configuration>
            <excludes>
              <exclude>ca/qc/ircm/proview/test/**/*.java</exclude>
              <exclude>ca/qc/ircm/proview/**/*ItTest.java</exclude>
            </excludes>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-failsafe-plugin</artifactId>
          <configuration>
            <includes>
              <include>ca/qc/ircm/proview/**/*ItTest.java</include>
            </includes>
            <systemPropertyVariables>
              <testbench.skip>${testbench.skip}</testbench.skip>
              <com.vaadin.testbench.Parameters.maxAttempts>${testbench.maxAttempts}
              </com.vaadin.testbench.Parameters.maxAttempts>
            </systemPropertyVariables>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-war-plugin</artifactId>
          <configuration>
            <packagingExcludes>
              %regex[WEB-INF/lib/slf4j-simple.*.jar],
              %regex[WEB-INF/lib/tomcat.*.jar]
            </packagingExcludes>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <configuration>
            <source>8</source>
          </configuration>
        </plugin>
        <plugin>
          <groupId>com.mysema.maven</groupId>
          <artifactId>apt-maven-plugin</artifactId>
          <version>${apt-maven-plugin.version}</version>
          <executions>
            <execution>
              <id>querydsl-metamodel</id>
              <goals>
                <goal>process</goal>
              </goals>
              <phase>generate-sources</phase>
              <configuration>
                <outputDirectory>target/generated-sources/querydsl</outputDirectory>
                <processor>com.querydsl.apt.jpa.JPAAnnotationProcessor</processor>
              </configuration>
            </execution>
          </executions>
          <dependencies>
            <dependency>
              <groupId>com.querydsl</groupId>
              <artifactId>querydsl-apt</artifactId>
              <version>${querydsl.version}</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>com.vaadin</groupId>
          <artifactId>vaadin-maven-plugin</artifactId>
          <version>${vaadin.version}</version>
          <executions>
            <execution>
              <goals>
                <goal>prepare-frontend</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.springframework.boot</groupId>
          <artifactId>spring-boot-maven-plugin</artifactId>
          <version>${spring-boot.version}</version>
          <configuration>
            <mainClass>${main-class}</mainClass>
            <executable>true</executable>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>repackage</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.mybatis.maven</groupId>
          <artifactId>migrations-maven-plugin</artifactId>
          <version>${migrations-maven-plugin.version}</version>
          <configuration>
            <repository>src/migration</repository>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.mariadb.jdbc</groupId>
              <artifactId>mariadb-java-client</artifactId>
              <version>${mariadb-java-client.version}</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>com.mycila</groupId>
          <artifactId>license-maven-plugin</artifactId>
          <configuration>
            <headerDefinitions>
              <headerDefinition>src/license/javacomment_style.xml</headerDefinition>
            </headerDefinitions>
            <mapping>
              <java>JAVACOMMENT_STYLE</java>
              <yml>SCRIPT_STYLE</yml>
              <ini>SEMICOLON_STYLE</ini>
              <scss>JAVADOC_STYLE</scss>
              <ldif>SCRIPT_STYLE</ldif>
            </mapping>
            <excludes>
              <exclude>**/.*</exclude>
              <exclude>**/*.txt</exclude>
              <exclude>**/*.ser</exclude>
              <exclude>*.log*</exclude>
              <exclude>node_modules/**</exclude>
              <exclude>package*.json</exclude>
              <exclude>webpack.*.js</exclude>
            </excludes>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>check</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <artifactId>maven-source-plugin</artifactId>
      </plugin>
      <plugin>
        <artifactId>maven-javadoc-plugin</artifactId>
      </plugin>
      <plugin>
        <artifactId>maven-failsafe-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>buildnumber-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>com.mysema.maven</groupId>
        <artifactId>apt-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>
  <reporting>
    <plugins>
      <plugin>
        <artifactId>maven-surefire-report-plugin</artifactId>
        <configuration>
          <reportsDirectories>
            <reportsDirectory>${project.build.directory}/surefire-reports</reportsDirectory>
            <reportsDirectory>${project.build.directory}/failsafe-reports</reportsDirectory>
          </reportsDirectories>
        </configuration>
      </plugin>
      <plugin>
        <groupId>com.github.spotbugs</groupId>
        <artifactId>spotbugs-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </reporting>
  <profiles>
    <profile>
      <id>vaadin-repository</id>
      <activation>
        <property>
          <name>vaadin-repository</name>
          <value>!false</value>
        </property>
      </activation>
      <repositories>
        <repository>
          <id>vaadin-addons</id>
          <url>https://maven.vaadin.com/vaadin-addons</url>
        </repository>
      </repositories>
    </profile>
    <profile>
      <id>production</id>
      <properties>
        <vaadin.productionMode>true</vaadin.productionMode>
      </properties>
      <build>
        <plugins>
          <plugin>
            <groupId>com.vaadin</groupId>
            <artifactId>vaadin-maven-plugin</artifactId>
            <version>${vaadin.version}</version>
            <executions>
              <execution>
                <goals>
                  <goal>build-frontend</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
    <profile>
      <id>firefox</id>
      <properties>
        <testbench.driver>org.openqa.selenium.firefox.FirefoxDriver</testbench.driver>
      </properties>
    </profile>
    <profile>
      <id>chrome</id>
      <properties>
        <testbench.driver>org.openqa.selenium.chrome.ChromeDriver</testbench.driver>
      </properties>
    </profile>
    <profile>
      <id>opera</id>
      <properties>
        <testbench.driver>org.openqa.selenium.opera.OperaDriver</testbench.driver>
      </properties>
    </profile>
  </profiles>
</project>
